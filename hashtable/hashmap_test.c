#include "hashmap.h"
#include <stdio.h>
void clear(void *v) {
	printf("clear - %ld\n",(long)v);
}
int main() {
	int size = 10,i;
	hashmap_t T;
	printf("creating hash table,size %d\n",size);
	hashmap_create(size,&T,clear);
	printf("initializing test cases\n");
	for(i=0;i<30;i++){
		char key[64];
		sprintf(key,"key%d",i);
		hashmap_set(&T, key, (void*)(long)i);
	}
	printf("total 30 nodes added\n");
	printf("removing 24 nodes\n");
	for(i=0;i<25;i++){
		char key[64];
		sprintf(key, "key%d",i);
		hashmap_remove(&T, key);
	}
	hashmap_iterator_t it;
	printf("clearing\n");
	hashmap_clear(&T);
	printf("show\n");
	for(
			hashmap_iterator_first(&T, &it);
			!hashmap_iterator_eof(&it); 
			hashmap_iterator_next(&T, &it)
	   ) {
		char *key; int v1,v2;
		key = hashmap_iterator_pair(&it,(void**)&v1);
		hashmap_get(&T,key,(void**)&v2);//hashmap_get出现在这里只是为了测试
		printf("%s\t=%d\t%d\n",key,v1,v2);
	}
	return 0;
}
