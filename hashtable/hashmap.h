#ifndef HASHMAP_H
#define HASHMAP_H
typedef void(*hashmap_clear_callback)(void*);
typedef struct node_s {
	char* key;
	void* value;
	struct node_s * next;
} node_t;

typedef struct hashmap_s {
	int size;
	hashmap_clear_callback clear_proc;
	node_t **vect;
} hashmap_t;

typedef struct hashmap_iterator_s {
	int idx;
	node_t * cur;
}hashmap_iterator_t;


int hashmap_create(unsigned int size, hashmap_t *ptab, hashmap_clear_callback clear);

void hashmap_set(hashmap_t *T,char *key,void* value);

int hashmap_get(hashmap_t *T,char *key,void **value) ;

void hashmap_remove(hashmap_t *T,char *key);

int hashmap_exists(hashmap_t *T,char*key);

void hashmap_clear(hashmap_t *T);

void hashmap_iterator_first(hashmap_t *T,hashmap_iterator_t *it);

void hashmap_iterator_next(hashmap_t *T,hashmap_iterator_t *it);

int hashmap_iterator_eof(hashmap_iterator_t *it);

char * hashmap_iterator_pair(hashmap_iterator_t *it,void** value);

#endif
