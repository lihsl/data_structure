#include "hashmap.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

typedef void(*hashmap_clear_callback)(void*);

static int hash(char *str);

/**
 * hash(str) 
 *
 * 散列函数，将字符串str进行散列
 */
static int hash(char *str) {
	int h = 0,i;
	for(i=0;str[i];i++) {
		h+=(int)str[i]<<((i%sizeof(int))*8);//累加之前需要移位，雪崩效应
	}
	h = h * 31 + 0;
	return h;
}

/**
 * hashmap_create(size, ptab, clear)
 *
 * 创建哈希表，长度为size，填写到到ptab 指向的内存
 * clear 是删除元素内存清理回调
 * 失败返回-1
 */
int hashmap_create(unsigned int size, hashmap_t *ptab, hashmap_clear_callback clear) {
	node_t **p;
	assert(ptab != NULL);
	p = (node_t **)malloc(size*sizeof(node_t*));
	if(p == NULL) {
		return -1;
	}
	memset(p, 0, size*sizeof(node_t*));
	ptab->size = size;
	ptab->vect = p;
	ptab->clear_proc = clear;
}

/**
 * hashmap_set(T,key,value)
 *
 * 向T所指向的哈希表添加键值对
 */
void hashmap_set(hashmap_t *T,char *key,void* value) {
	int pos;
	assert(T);
	assert(key);

    pos = (unsigned int)hash(key) % T->size; //将散列值转化为 向量表下标
	//printf("pos = %d\n",pos);
	node_t ** p;
	for(p=T->vect+pos; *p; p=&(*p)->next) {//遍历元素指向的链表,判断key是否已经存在
		//printf("p = %p\n",p);
		if(strcmp((*p)->key, key) == 0)
			break;
	}
	if(*p == NULL) { //key不存在，则创建新的节点
		node_t *q = malloc(sizeof(node_t));
		q->next = *p;
		*p = q;
	}
	(*p)->key = strdup(key);//拷贝key
	assert((*p)->key);
	(*p)->value = value;
}
/**
 * hashmap_get(T,key,value)
 *
 * 从T指向的哈希表中获取键为key的元素，并返回到指针value指向的内存
 * 如果元素不存在返回 0，否则返回1
 */
int hashmap_get(hashmap_t *T,char *key,void **value) {
	assert(T);
	assert(key);
	assert(value);
	int pos = (unsigned int)hash(key) % T->size;
	node_t ** p;
	for(p=T->vect+pos; *p; p=&((*p)->next)) {
		if(strcmp((*p)->key,key) == 0)
			break;
	}
	if(*p == NULL) return 0;
	if(value) *value = (*p)->value;
	return 1;
}


void hashmap_remove(hashmap_t *T,char *key) {
	assert(T);
	assert(key);
	int pos = (unsigned int)hash(key) % T->size;
	node_t ** p;
	for(p=T->vect+pos; *p; p=&((*p)->next)) {
		if(strcmp((*p)->key, key) == 0) {
			if(T->clear_proc)
				T->clear_proc((*p)->value);
			node_t *q=(*p)->next;
			free(*p);
			*p = q;
			break;
		}
	}

}

/**
 * hashmap_exists(T,key)
 *
 * 判断key是否在T指向的哈希表中
 * 存在返回1，否则返回0
 */
int hashmap_exists(hashmap_t *T, char*key) {
	assert(T != NULL);
	assert(key != NULL);
	return hashmap_get(T, key, NULL);
}


void hashmap_clear(hashmap_t *T) {
	assert(T != NULL);
	int i;
	for(i=0; i<T->size; i++) {
		node_t ** p;
		for(p=T->vect+i; *p; p=&((*p)->next)) {
			if(T->clear_proc)
				T->clear_proc((*p)->value);
			node_t *q = (*p)->next;
			free(*p);
			*p = q;
			break;
		}
	}
}

/**
 * hashmap_iterator_first(T,it)
 *
 * 根据哈希表指针T，获取第一个元素的迭代器
 */
void hashmap_iterator_first(hashmap_t *T,hashmap_iterator_t *it) {
	node_t * p;
	assert(T != NULL);
	assert(it != NULL);

	int i;
	p = *(T->vect);
	if(p == NULL) {
		for(i=1; i<T->size; i++) {
			if(*(T->vect + i) != NULL) {
				p = *(T->vect + i);
				break;
			}
		}
	}

	if(p == NULL) {
		it->idx = -1;
	} else {
		it->idx = i;
		it->cur = p;
	}
	//printf("hashmap_iterator_first idx:%d cur:%p\n",i,p);

}

/**
 * hashmap_iterator_next(T,it) 
 *
 * 根据哈希表T，从it中获取下一个元素的迭代器并返回到it
 */
void hashmap_iterator_next(hashmap_t *T,hashmap_iterator_t *it) {
	hashmap_iterator_t inext;
	assert(T);
	assert(it);

	inext.idx = it->idx;//初始化返回值idx为当前idx

	if(it->idx < 0) {//如果迭代器有结尾标记，继续传递结尾标记
		inext.idx = -1;
	} else {
		node_t * p;
		int i;

		p = it->cur->next;//指向下一个元素
		if(p == NULL) {//如果当前元素防重链表已经到尾部，则依次向后搜索HASH表，直到找到有效链表节点
			for(i=it->idx+1; i<T->size; i++) {
				if(*(T->vect + i) != NULL) {
					inext.idx = i;//待返回的idx设为当前向量表索引
					p = *(T->vect + i);//有效节点指针
					break;
				}
			}
		}

		if(p == NULL) {//并没有找到
			inext.idx = -1;
		} else {
			inext.cur = p;
		}
	}
	it->idx = inext.idx;
	it->cur = inext.cur;
}

/**
 * hashmap_iterator_eofa(it)
 *
 * 判断迭代器指针it是否到达结尾
 */
int hashmap_iterator_eof(hashmap_iterator_t *it) {
	assert(it != NULL);
	if(it->idx<0) return 1;
	return 0;
}

/**
 * hashmap_iterator_pair(it,value)
 *
 * 从迭代器获取引用的键值对，返回值为键(Key name)
 * 如果it指向结尾，将会断言失败
 */
char * hashmap_iterator_pair(hashmap_iterator_t *it,void ** value) {
	assert(it != NULL);
	assert(it->idx >= 0);

	if(value)
		*value = it->cur->value;

	return it->cur->key;
}

